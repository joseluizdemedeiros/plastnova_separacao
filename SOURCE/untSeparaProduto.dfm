object frmSeparaProduto: TfrmSeparaProduto
  Left = -1
  Top = 130
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Separa'#231#227'o de Produto'
  ClientHeight = 741
  ClientWidth = 1261
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1261
    Height = 741
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 1259
      Height = 666
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object pnlTopo: TPanel
        Left = 1
        Top = 1
        Width = 1253
        Height = 40
        Align = alTop
        TabOrder = 0
        object Label7: TLabel
          Left = 6
          Top = 0
          Width = 122
          Height = 39
          Caption = 'Pedido:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edtPedido: TDBText
          Left = 134
          Top = 0
          Width = 199
          Height = 39
          DataField = 'PEDV'
          DataSource = dtPedidoProd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 395
          Top = 3
          Width = 95
          Height = 35
          Alignment = taCenter
          Caption = 'Carga:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -29
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblCarga: TLabel
          Left = 532
          Top = 0
          Width = 9
          Height = 39
        end
      end
      object pnlDados: TPanel
        Left = 390
        Top = 41
        Width = 864
        Height = 517
        Align = alClient
        TabOrder = 1
        DesignSize = (
          864
          517)
        object DBText1: TDBText
          Left = 268
          Top = 368
          Width = 113
          Height = 39
          Alignment = taCenter
          DataField = 'ESTOQUE_REAL'
          DataSource = dtEstoque
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText4: TDBText
          Left = 271
          Top = 210
          Width = 110
          Height = 39
          Alignment = taCenter
          Color = clBtnFace
          DataField = 'VENDIDO'
          DataSource = dtPedidoProd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object DBText5: TDBText
          Left = 268
          Top = 285
          Width = 113
          Height = 39
          Alignment = taCenter
          DataField = 'SEPARADO'
          DataSource = dtPedidoProd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edtBarra: TDBText
          Left = 214
          Top = 124
          Width = 320
          Height = 39
          DataField = 'CBARRA'
          DataSource = dtPedidoProd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 13
          Top = 210
          Width = 228
          Height = 39
          Caption = 'Qtde Vendida:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 12
          Top = 124
          Width = 196
          Height = 39
          Caption = 'C'#243'd. Barras:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 13
          Top = 285
          Width = 249
          Height = 39
          Caption = 'Qtde Separado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 13
          Top = 368
          Width = 233
          Height = 39
          Caption = 'Estoque Atual:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 12
          Top = 12
          Width = 140
          Height = 39
          Caption = 'Produto:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edtProd: TDBText
          Left = 155
          Top = 12
          Width = 127
          Height = 39
          DataField = 'COD_PROD'
          DataSource = dtPedidoProd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText3: TDBText
          Left = 13
          Top = 66
          Width = 866
          Height = 33
          DataField = 'DESC_PROD'
          DataSource = dtPedidoProd
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btBusca: TButton
          Left = 800
          Top = 20
          Width = 57
          Height = 41
          Caption = 'btBusca'
          TabOrder = 0
          Visible = False
          OnClick = btBuscaClick
        end
        object Panel3: TPanel
          Left = 1
          Top = 480
          Width = 862
          Height = 36
          Align = alBottom
          BevelKind = bkFlat
          BevelOuter = bvNone
          TabOrder = 1
          object DBText2: TDBText
            Left = 4
            Top = 1
            Width = 121
            Height = 31
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'PULMAO'
            DataSource = dtPedidoProd
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -24
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object DBText6: TDBText
            Left = 116
            Top = 1
            Width = 127
            Height = 31
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'PULMAO2'
            DataSource = dtPedidoProd
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -24
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object DBText7: TDBText
            Left = 237
            Top = 1
            Width = 127
            Height = 31
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'PULMAO3'
            DataSource = dtPedidoProd
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -24
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object DBText8: TDBText
            Left = 370
            Top = 1
            Width = 127
            Height = 31
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'PULMAO4'
            DataSource = dtPedidoProd
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -24
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object DBText9: TDBText
            Left = 509
            Top = 1
            Width = 127
            Height = 31
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'PULMAO5'
            DataSource = dtPedidoProd
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -24
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
        end
        object Panel4: TPanel
          Left = 1
          Top = 452
          Width = 862
          Height = 28
          Align = alBottom
          Color = clBackground
          ParentBackground = False
          TabOrder = 2
          object Label10: TLabel
            Left = 38
            Top = 1
            Width = 58
            Height = 25
            Alignment = taCenter
            Caption = 'PL 01'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label11: TLabel
            Left = 151
            Top = 1
            Width = 58
            Height = 25
            Alignment = taCenter
            Caption = 'PL 02'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 273
            Top = 1
            Width = 58
            Height = 25
            Alignment = taCenter
            Caption = 'PL 03'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 398
            Top = 1
            Width = 58
            Height = 25
            Alignment = taCenter
            Caption = 'PL 04'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label14: TLabel
            Left = 543
            Top = 1
            Width = 58
            Height = 25
            Alignment = taCenter
            Caption = 'PL 05'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object pnlEtiqueta: TPanel
          Left = 447
          Top = 320
          Width = 410
          Height = 103
          Anchors = [akRight, akBottom]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          Visible = False
          object Label16: TLabel
            Left = 16
            Top = 19
            Width = 62
            Height = 19
            Caption = 'Pedido:'
          end
          object Label17: TLabel
            Left = 264
            Top = 19
            Width = 67
            Height = 19
            Caption = 'Volume:'
          end
          object Label18: TLabel
            Left = 22
            Top = 67
            Width = 54
            Height = 19
            Caption = 'Carga:'
          end
          object Button1: TButton
            Left = 283
            Top = 62
            Width = 118
            Height = 36
            Caption = 'Imprimir'
            TabOrder = 0
            OnClick = Button1Click
          end
          object edtPedidoEtiqueta: TEdit
            Left = 84
            Top = 16
            Width = 125
            Height = 27
            Color = clMenu
            ReadOnly = True
            TabOrder = 1
          end
          object edtVolumeEtiqueta: TEdit
            Left = 337
            Top = 16
            Width = 64
            Height = 27
            NumbersOnly = True
            TabOrder = 2
          end
          object edtCarga: TEdit
            Left = 84
            Top = 64
            Width = 79
            Height = 27
            Color = clMenu
            NumbersOnly = True
            ReadOnly = True
            TabOrder = 3
          end
        end
      end
      object pnlFiltro: TPanel
        Left = 1
        Top = 558
        Width = 1253
        Height = 103
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object Label6: TLabel
          Left = 6
          Top = 18
          Width = 503
          Height = 35
          Caption = 'Insira um C'#243'digo ou passe o leitor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -29
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object pnlQtde: TPanel
          Left = 605
          Top = 0
          Width = 648
          Height = 103
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label8: TLabel
            Left = 14
            Top = 15
            Width = 69
            Height = 35
            Caption = 'Qtde'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -29
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edtQtde: TEdit
            Left = 14
            Top = 53
            Width = 102
            Height = 47
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnKeyPress = edtQtdeKeyPress
          end
        end
        object edtCodigo: TEdit
          Left = 6
          Top = 53
          Width = 593
          Height = 47
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          NumbersOnly = True
          ParentFont = False
          TabOrder = 1
          OnKeyDown = edtCodigoKeyDown
        end
      end
      object pnlGrid: TPanel
        Left = 1
        Top = 41
        Width = 389
        Height = 517
        Align = alLeft
        TabOrder = 3
        object gridPedido: TDBGrid
          Left = 1
          Top = 1
          Width = 387
          Height = 515
          Align = alClient
          Color = clInactiveBorder
          DataSource = dtPedidoProd
          DrawingStyle = gdsGradient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -16
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          Columns = <
            item
              ButtonStyle = cbsNone
              Expanded = False
              FieldName = 'COD_PROD'
              Title.Caption = 'C'#243'digo'
              Width = 106
              Visible = True
            end
            item
              Alignment = taCenter
              ButtonStyle = cbsNone
              Expanded = False
              FieldName = 'VENDIDO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -32
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Vendido'
              Width = 84
              Visible = True
            end
            item
              Alignment = taCenter
              ButtonStyle = cbsNone
              Expanded = False
              FieldName = 'SEPARADO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -32
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Separado'
              Width = 83
              Visible = True
            end
            item
              Alignment = taCenter
              ButtonStyle = cbsNone
              Expanded = False
              FieldName = 'FALTA'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -32
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Falta'
              Width = 88
              Visible = True
            end>
        end
      end
    end
    object pnlButon: TPanel
      Left = 1
      Top = 667
      Width = 1259
      Height = 73
      Align = alBottom
      BevelInner = bvLowered
      BevelKind = bkFlat
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        1255
        69)
      object lblVolume: TLabel
        Left = 199
        Top = 28
        Width = 4
        Height = 16
        Anchors = [akLeft, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 48
      end
      object Label15: TLabel
        Left = 954
        Top = 51
        Width = 293
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = 'F5 - Fechar Volume | F6 -  Imprimir etiqueta volume'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 958
        ExplicitTop = 55
      end
      object btFechar: TBitBtn
        Left = 7
        Top = 4
        Width = 106
        Height = 62
        Caption = 'Sair'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Glyph.Data = {
          360C0000424D360C000000000000360000002800000020000000200000000100
          180000000000000C0000120B0000120B00000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FF0732DE0732DEFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FF0732DE0732DE0732DE0732DEFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FF0732DE0732DE0732DE0732DE0732DEFF00FFFF00FFFF00FF
          0533EC0533EBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FF0732DE0732DE0732DE0732DE0732DEFF00FFFF00FFFF00FF0534EE
          0534ED0533EC0533EBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF0732DE0732DE0732DE0732DE0732DEFF00FFFF00FFFF00FFFF00FF0534EF
          0534EE0534ED0533EC0533EBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF07
          32DE0732DE0732DE0732DE0732DEFF00FFFF00FFFF00FFFF00FFFF00FF0534F0
          0534EF0534EE0534ED0533EC0533ECFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0732DE07
          32DE0732DE0732DE0732DEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          0534F00534EF0534EE0534EE0534ED0533ECFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0732E00732DF07
          32DF0732DE0732DEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FF0434F00534EF0534EF0534EE0534ED0533ECFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0632E20732E10732E007
          32E00732DFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FF0434F00534F00534EF0534EE0534ED0533ECFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0633E40633E30632E20732E207
          32E1FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF0434F00534F00534EF0534EE0534ED0533ECFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FF0633E60633E50633E40633E30632E3FF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FF0434F10534F00534EF0534EE0534EE0534EDFF00
          FFFF00FFFF00FFFF00FFFF00FF0633E80633E70633E60633E50633E4FF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F00534EF0534EF0534EE0534
          EDFF00FFFF00FFFF00FF0533EA0633E90633E80633E70633E60633E5FF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F00534F00534EF0534
          EE0534EDFF00FF0533EB0533EB0533EA0533E90633E80633E7FF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F10534F00534
          EF0534EE0534ED0533EC0533EC0533EB0533EA0533E9FF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F10534
          F00534EF0534EE0534EE0534ED0533EC0533EBFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434
          F10434F00534EF0534EF0534EE0534EDFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F30434
          F20434F10434F00534F00534EF0534EEFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F50434F40434
          F30434F20434F10434F10534F00534EF0534EEFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0435F70434F60434F50434
          F40434F30434F2FF00FF0434F10534F00534EF0534EEFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0335F90335F80435F70434F60434
          F50434F4FF00FFFF00FFFF00FF0434F10434F00534EF0534EEFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FF0335FA0335FA0335F90335F80435F70434
          F6FF00FFFF00FFFF00FFFF00FFFF00FF0434F10434F00534F00534EFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FF0335FB0335FB0335FB0335FA0335F90335F8FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F10434F10534F00534EFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF0335FB0335FB0335FB0335FB0335FB0335FAFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F10434F10534F005
          34EFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FF0335FB0335FB0335FB0335FB0335FB0335FBFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0434F105
          34F00534EFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FF0335FB0335FB0335FB0335FB0335FB0335FBFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF04
          34F10434F00534EFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          0335FB0335FB0335FB0335FB0335FB0335FB0335FBFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF0434F10434F00534F0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0335FB
          0335FB0335FB0335FB0335FB0335FB0335FBFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FF0434F10434F10534F0FF00FFFF00FFFF00FFFF00FF0335FB0335FB
          0335FB0335FB0335FB0335FB0335FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FF0434F1FF00FFFF00FFFF00FFFF00FFFF00FF0335FB0335FB
          0335FB0335FB0335FB0335FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0335FB0335FB
          0335FB0335FB0335FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0335FB
          0335FB0335FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        ParentFont = False
        TabOrder = 0
        OnClick = btFecharClick
      end
    end
  end
  object IBSepara: TIBDataSet
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    UniDirectional = False
    Left = 1063
    Top = 260
  end
  object IBItemPedidoProd: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select p.pedv,I.cbarra,I.desc_prod,I.cod_prod,I.unidade,'
      
        'Pr.estoque_real,pr.pulmao,pr.pulmao2,pr.pulmao3,pr.pulmao4,pr.pu' +
        'lmao5,'
      'sum(coalesce(I.qtde_vendido,0))as vendido, '
      'sum(coalesce(I.qtde_separado,0))as separado,'
      'sum(coalesce(I.qtde_vendido-I.qtde_separado,0))as falta'
      'from item_pedido I'
      '   inner join pedido p on (I.pedv = p.pedv)'
      '   inner join item_carga ig on (p.pedv = ig.id_pedido)'
      '   inner join carga cg on (ig.id_carga = cg.id_carga)'
      '   inner join produtos pr on(pr.cod = I.cod_prod)'
      'where cg.id_carga = :carga'
      'and I.pedv             = :pedido'
      'group by'
      
        '   p.pedv,I.cbarra,cg.motorista, cg.data_saida, cg.id_carga, I.d' +
        'esc_prod,'
      
        ' I.cod_prod,I.unidade,Pr.estoque_real,pr.pulmao,pr.pulmao2,pr.pu' +
        'lmao3,pr.pulmao4,pr.pulmao5'
      'order by'
      'p.pedv')
    Left = 903
    Top = 260
    ParamData = <
      item
        DataType = ftInteger
        Name = 'carga'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'pedido'
        ParamType = ptUnknown
      end>
    object IBItemPedidoProdPEDV: TIBStringField
      FieldName = 'PEDV'
      Origin = '"PEDIDO"."PEDV"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 6
    end
    object IBItemPedidoProdVENDIDO: TIBBCDField
      FieldName = 'VENDIDO'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object IBItemPedidoProdSEPARADO: TIBBCDField
      FieldName = 'SEPARADO'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object IBItemPedidoProdCOD_PROD: TIBStringField
      FieldName = 'COD_PROD'
      Origin = '"ITEM_PEDIDO"."COD_PROD"'
      FixedChar = True
      Size = 4
    end
    object IBItemPedidoProdDESC_PROD: TIBStringField
      FieldName = 'DESC_PROD'
      Origin = '"ITEM_PEDIDO"."DESC_PROD"'
      Size = 100
    end
    object IBItemPedidoProdCBARRA: TIBStringField
      FieldName = 'CBARRA'
      Origin = '"ITEM_PEDIDO"."CBARRA"'
      Size = 13
    end
    object IBItemPedidoProdFALTA: TIBBCDField
      FieldName = 'FALTA'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object IBItemPedidoProdUNIDADE: TIBStringField
      FieldName = 'UNIDADE'
      Origin = '"ITEM_PEDIDO"."UNIDADE"'
      Size = 15
    end
    object IBItemPedidoProdESTOQUE_REAL: TIntegerField
      FieldName = 'ESTOQUE_REAL'
      Origin = '"PRODUTOS"."ESTOQUE_REAL"'
    end
    object IBItemPedidoProdPULMAO: TIBStringField
      FieldName = 'PULMAO'
      Origin = '"PRODUTOS"."PULMAO"'
      Size = 5
    end
    object IBItemPedidoProdPULMAO2: TIBStringField
      FieldName = 'PULMAO2'
      Origin = '"PRODUTOS"."PULMAO2"'
      Size = 5
    end
    object IBItemPedidoProdPULMAO3: TIBStringField
      FieldName = 'PULMAO3'
      Origin = '"PRODUTOS"."PULMAO3"'
      Size = 5
    end
    object IBItemPedidoProdPULMAO4: TIBStringField
      FieldName = 'PULMAO4'
      Origin = '"PRODUTOS"."PULMAO4"'
      Size = 5
    end
    object IBItemPedidoProdPULMAO5: TIBStringField
      FieldName = 'PULMAO5'
      Origin = '"PRODUTOS"."PULMAO5"'
      Size = 5
    end
  end
  object dtPedidoProd: TDataSource
    DataSet = IBItemPedidoProd
    Left = 687
    Top = 140
  end
  object IBConfig: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select * from CONFIG')
    Left = 775
    Top = 140
    object IBConfigID: TIntegerField
      FieldName = 'ID'
      Origin = '"CONFIG"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IBConfigTEMPO_ATUALIZAR: TIBBCDField
      FieldName = 'TEMPO_ATUALIZAR'
      Origin = '"CONFIG"."TEMPO_ATUALIZAR"'
      Precision = 9
      Size = 2
    end
    object IBConfigQTDE_ENTRADA: TIntegerField
      FieldName = 'QTDE_ENTRADA'
      Origin = '"CONFIG"."QTDE_ENTRADA"'
    end
    object IBConfigACEITAR_NEGATIVO_SAIDA: TIBStringField
      FieldName = 'ACEITAR_NEGATIVO_SAIDA'
      Origin = '"CONFIG"."ACEITAR_NEGATIVO_SAIDA"'
      FixedChar = True
      Size = 1
    end
  end
  object IBEstoque: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    DataSource = dtPedidoProd
    ParamCheck = True
    SQL.Strings = (
      'select estoque_real from produtos'
      'where cod = :cod_prod')
    Left = 871
    Top = 140
    ParamData = <
      item
        DataType = ftString
        Name = 'cod_prod'
        ParamType = ptInput
      end>
    object IBEstoqueESTOQUE_REAL: TIntegerField
      FieldName = 'ESTOQUE_REAL'
      Origin = '"PRODUTOS"."ESTOQUE_REAL"'
    end
  end
  object dtEstoque: TDataSource
    DataSet = IBEstoque
    Left = 999
    Top = 148
  end
  object Timer1: TTimer
    Interval = 5000
    OnTimer = Timer1Timer
    Left = 967
    Top = 148
  end
  object frxReportCarga: TfrxReport
    Version = '5.3.16'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.CreateDate = 43144.812827905100000000
    ReportOptions.LastChange = 43144.812827905100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 833
    Top = 473
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 90.000000000000000000
      PaperHeight = 50.000000000000000000
      PaperSize = 256
      object Memo1: TfrxMemoView
        Left = 2.000000000000000000
        Top = 2.000000000000000000
        Width = 337.047310000000000000
        Height = 57.622047240000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -45
        Font.Name = 'Arial Black'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[PEDIDO]')
        ParentFont = False
      end
      object Memo2: TfrxMemoView
        Left = 7.000000000000000000
        Top = 74.000000000000000000
        Width = 187.488250000000000000
        Height = 30.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[CARGA]')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Left = 212.000000000000000000
        Top = 74.000000000000000000
        Width = 123.488250000000000000
        Height = 30.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VOLUME]')
        ParentFont = False
      end
      object Memo4: TfrxMemoView
        Left = 6.000000000000000000
        Top = 124.000000000000000000
        Width = 329.488250000000000000
        Height = 21.118120000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[CLIENTE]')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 6.000000000000000000
        Top = 159.000000000000000000
        Width = 330.267780000000000000
        Height = 18.897650000000000000
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[CIDADE]')
        ParentFont = False
      end
    end
  end
  object frxReportProdPedido: TfrxReport
    Version = '5.3.16'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.CreateDate = 43144.856806713000000000
    ReportOptions.LastChange = 43144.856806713000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 871
    Top = 258
    Datasets = <
      item
        DataSet = frxDBProdPedido
        DataSetName = 'frxDBProdPedido'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 90.000000000000000000
      PaperHeight = 50.000000000000000000
      PaperSize = 256
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 23.677180000000000000
        Top = 18.897650000000000000
        Width = 340.157700000000000000
        DataSet = frxDBProdPedido
        DataSetName = 'frxDBProdPedido'
        RowCount = 0
        object frxDBProdPedidoCOD: TfrxMemoView
          Left = 5.000000000000000000
          Top = 4.102350000000001000
          Width = 48.456710000000000000
          Height = 15.118120000000000000
          DataField = 'COD'
          DataSet = frxDBProdPedido
          DataSetName = 'frxDBProdPedido'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBProdPedido."COD"]')
          ParentFont = False
        end
        object frxDBProdPedidoDESCRICAO: TfrxMemoView
          Left = 56.000000000000000000
          Top = 4.102350000000001000
          Width = 241.409710000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRICAO'
          DataSet = frxDBProdPedido
          DataSetName = 'frxDBProdPedido'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBProdPedido."DESCRICAO"]')
          ParentFont = False
        end
        object frxDBProdPedidoQTDE: TfrxMemoView
          Left = 300.000000000000000000
          Top = 4.102350000000001000
          Width = 38.370130000000000000
          Height = 15.118120000000000000
          DataField = 'QTDE'
          DataSet = frxDBProdPedido
          DataSetName = 'frxDBProdPedido'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBProdPedido."QTDE"]')
          ParentFont = False
        end
      end
    end
  end
  object IBProdPedido: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select'
      'p.cod,'
      'p.descricao,'
      'iv.id_volume,'
      'iv.qtde'
      ''
      'from  item_volumes iv'
      'inner join produtos p on(p.cod = iv.id_prod)'
      'where'
      'iv.id_pedido = :pedido'
      'and'
      'iv.id_volume = :volume'
      'order by p.descricao')
    Left = 991
    Top = 482
    ParamData = <
      item
        DataType = ftString
        Name = 'pedido'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'volume'
        ParamType = ptInput
      end>
    object IBProdPedidoCOD: TIBStringField
      FieldName = 'COD'
      Origin = '"PRODUTOS"."COD"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 4
    end
    object IBProdPedidoDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"PRODUTOS"."DESCRICAO"'
      Size = 100
    end
    object IBProdPedidoID_VOLUME: TIntegerField
      FieldName = 'ID_VOLUME'
      Origin = '"ITEM_VOLUMES"."ID_VOLUME"'
    end
    object IBProdPedidoQTDE: TIntegerField
      FieldName = 'QTDE'
      Origin = '"ITEM_VOLUMES"."QTDE"'
    end
  end
  object frxDBProdPedido: TfrxDBDataset
    UserName = 'frxDBProdPedido'
    CloseDataSource = False
    DataSet = IBProdPedido
    BCDToCurrency = False
    Left = 999
    Top = 434
  end
end
