program PlastClient;

uses
  Forms,
  untPrincipal in 'untPrincipal.pas' {FrmPrincipal},
  untDMDados in 'untDMDados.pas' {DMDados: TDataModule},
  untLogin in 'untLogin.pas' {frmLogin},
  untErro in 'untErro.pas' {frmAtencao},
  untSeparaProduto in 'untSeparaProduto.pas' {frmSeparaProduto},
  untExcessoProd in 'untExcessoProd.pas' {FrmExcessoProd},
  untErroEstoque in 'untErroEstoque.pas' {frmErroEstoque};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'PlastNova';
  Application.CreateForm(TDMDados, DMDados);
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmAtencao, frmAtencao);
  Application.CreateForm(TfrmSeparaProduto, frmSeparaProduto);
  Application.CreateForm(TFrmExcessoProd, FrmExcessoProd);
  Application.CreateForm(TfrmErroEstoque, frmErroEstoque);
  Application.Run;
end.
