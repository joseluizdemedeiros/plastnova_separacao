unit untErroEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, IBX.IBCustomDataSet, IBX.IBQuery, Vcl.Imaging.pngimage, Vcl.StdCtrls;

type
  TfrmErroEstoque = class(TForm)
    Panel1: TPanel;
    Timer1: TTimer;
    IBConfig: TIBQuery;
    IBConfigID: TIntegerField;
    IBConfigACEITAR_NEGATIVO_SAIDA: TIBStringField;
    IBConfigTEMPO_ATUALIZAR: TIBBCDField;
    IBConfigQTDE_ENTRADA: TIntegerField;
    Image1: TImage;
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmErroEstoque: TfrmErroEstoque;

implementation

uses untDMDados;

{$R *.dfm}

procedure TfrmErroEstoque.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 if Key = VK_ESCAPE then
  Close;
end;

procedure TfrmErroEstoque.Timer1Timer(Sender: TObject);
begin
IBConfig.Close;
IBConfig.Open;

if(IBConfigACEITAR_NEGATIVO_SAIDA.asString = 'S')then
  close;
end;

end.
