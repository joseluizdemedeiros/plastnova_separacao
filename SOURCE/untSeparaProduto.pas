unit untSeparaProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, DBCtrls, DB, IBX.IBCustomDataSet,IBX.IBQuery,
  Grids, DBGrids, frxClass, frxDBSet;

type
  TfrmSeparaProduto = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    pnlButon: TPanel;
    btFechar: TBitBtn;
    DBText4: TDBText;
    DBText5: TDBText;
    Label3: TLabel;
    edtBarra: TDBText;
    IBSepara: TIBDataSet;
    IBItemPedidoProd: TIBQuery;
    dtPedidoProd: TDataSource;
    IBItemPedidoProdPEDV: TIBStringField;
    IBItemPedidoProdVENDIDO: TIBBCDField;
    IBItemPedidoProdSEPARADO: TIBBCDField;
    IBItemPedidoProdCOD_PROD: TIBStringField;
    IBItemPedidoProdDESC_PROD: TIBStringField;
    IBItemPedidoProdCBARRA: TIBStringField;
    IBItemPedidoProdFALTA: TIBBCDField;
    IBItemPedidoProdUNIDADE: TIBStringField;
    btBusca: TButton;
    IBConfig: TIBQuery;
    IBConfigID: TIntegerField;
    IBConfigTEMPO_ATUALIZAR: TIBBCDField;
    IBConfigQTDE_ENTRADA: TIntegerField;
    IBConfigACEITAR_NEGATIVO_SAIDA: TIBStringField;
    IBItemPedidoProdESTOQUE_REAL: TIntegerField;
    Label9: TLabel;
    DBText1: TDBText;
    IBEstoque: TIBQuery;
    dtEstoque: TDataSource;
    IBEstoqueESTOQUE_REAL: TIntegerField;
    Timer1: TTimer;
    IBItemPedidoProdPULMAO: TIBStringField;
    IBItemPedidoProdPULMAO2: TIBStringField;
    IBItemPedidoProdPULMAO3: TIBStringField;
    IBItemPedidoProdPULMAO4: TIBStringField;
    IBItemPedidoProdPULMAO5: TIBStringField;
    pnlTopo: TPanel;
    Label7: TLabel;
    edtPedido: TDBText;
    pnlDados: TPanel;
    pnlFiltro: TPanel;
    edtCodigo: TEdit;
    pnlQtde: TPanel;
    Label8: TLabel;
    edtQtde: TEdit;
    Label6: TLabel;
    pnlGrid: TPanel;
    gridPedido: TDBGrid;
    Label4: TLabel;
    Label1: TLabel;
    edtProd: TDBText;
    DBText3: TDBText;
    Panel3: TPanel;
    Panel4: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBText2: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBText9: TDBText;
    lblCarga: TLabel;
    lblVolume: TLabel;
    Label15: TLabel;
    frxReportCarga: TfrxReport;
    frxReportProdPedido: TfrxReport;
    IBProdPedido: TIBQuery;
    IBProdPedidoCOD: TIBStringField;
    IBProdPedidoDESCRICAO: TIBStringField;
    IBProdPedidoID_VOLUME: TIntegerField;
    IBProdPedidoQTDE: TIntegerField;
    frxDBProdPedido: TfrxDBDataset;
    pnlEtiqueta: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Button1: TButton;
    edtPedidoEtiqueta: TEdit;
    edtVolumeEtiqueta: TEdit;
    Label18: TLabel;
    edtCarga: TEdit;
    procedure btFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure rdbOpcaoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtQtdeKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure btBuscaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SetaProduto;
    procedure edtCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }

   procedure CriaVolume(vPedido:String; vCarga: Integer);
   procedure InserirItemVolume(volume: Integer; qtd: Double);
   function ExisteItemVolume(volume: Integer; pedido,produto: String): Boolean;
   function ExisteVolume(pedido: String): Boolean;overload;
   function ExisteVolume(pedido: String;carga:integer;volume: integer): Boolean;overload;

   function PegaVolume(vPedido: String; vCarga: Integer; tipo: String): Integer;

  public
   procedure separaProduto;
   function existeProd: Boolean;
   procedure AlteraEstoque(pProd: String;Pqtde: Integer);
   function ConfereEstoque(pProd: String):Integer;

   function ConfereQtdeSeparado(vPed,vProd: String): Double;

   procedure FecharVolume(vPedido: String);

   procedure ImprimirEtiquetas;

   function PegaDadosCliente(vPedido,vCampo: String): String;

      { Public declarations }
  end;

var
  frmSeparaProduto: TfrmSeparaProduto;
  //controla qtde separacao maxima
  qtde_vend,qtde_separa:Double;
  vProd,vBarra: String;
  tipo: String;
  estoque: Integer;

 implementation

uses untPrincipal, untErro, untDMDados,untErroEstoque, untExcessoProd;

{$R *.dfm}

procedure TfrmSeparaProduto.btFecharClick(Sender: TObject);
begin
if ExisteVolume(IBItemPedidoProdPEDV.AsString) = True then
 begin
   ShowMessage('O pedido ' + '[ ' + edtPedido.Caption + ' ]' + ', possui um volume em aberto' + #13#13 + 'Termine a separa��o entes de sair...');
   Abort;
 end;
Close;
end;

procedure TfrmSeparaProduto.Button1Click(Sender: TObject);
begin
if ExisteVolume(edtPedidoEtiqueta.Text,StrToInt(edtCarga.Text),StrToInt(edtVolumeEtiqueta.Text)) = False then
 begin
   ShowMessage('N�o existe volume fechado desse pedido...');
   edtPedidoEtiqueta.Clear;
   edtCarga.Clear;
   edtVolumeEtiqueta.Clear;
   pnlEtiqueta.Visible:= False;
   Abort;
  end;

   // etiqueta de carga
    frxReportCarga.Variables['pedido'] := QuotedStr('PED: ' + edtPedidoEtiqueta.Text);
    frxReportCarga.Variables['carga'] := QuotedStr('CARGA: ' + edtCarga.Text);
    frxReportCarga.Variables['volume'] := QuotedStr('VOL: ' + edtVolumeEtiqueta.Text);
    frxReportCarga.Variables['cliente'] := QuotedStr(PegaDadosCliente(edtPedidoEtiqueta.Text, 'nome'));
    frxReportCarga.Variables['cidade'] := QuotedStr(PegaDadosCliente(edtPedidoEtiqueta.Text, 'cidade') + ' - ' +
      PegaDadosCliente(edtPedidoEtiqueta.Text, 'uf'));
    frxReportCarga.PrepareReport();
    frxReportCarga.Print;

    // itens separados
    IBProdPedido.Close;
    IBProdPedido.ParamByName('pedido').AsString := edtPedidoEtiqueta.Text;
    IBProdPedido.ParamByName('volume').AsInteger :=  StrToInt(edtVolumeEtiqueta.Text);
    frxReportProdPedido.PrepareReport();
    frxReportProdPedido.Print;
    edtPedidoEtiqueta.Clear;
    edtCarga.Clear;
    edtVolumeEtiqueta.Clear;
    pnlEtiqueta.Visible:= False;
end;

function TfrmSeparaProduto.ConfereEstoque(pProd: String): Integer;
var
  qry: TIBQuery;
begin
  qry := TIBQuery.Create(nil);
  qry.Database := DMDados.IBDatabase;
  try
  qry.Close;
  qry.SQL.Clear;

  qry.SQL.Add('select cod,cbarra,estoque_real from produtos');

  if Length(edtCodigo.Text) <= 6 then
      qry.SQL.Add('where cod = :pProd')
      else
     if Length(edtCodigo.Text) > 6  then
       qry.SQL.Add('where cbarra = :pProd');

    qry.Params[0].AsString := pProd;
    qry.Open;
    Result := qry.FieldByName('estoque_real').AsInteger;

   qry.Close;
  finally
   qry.Close;
   qry.Free;
  end;
end;

function TfrmSeparaProduto.ConfereQtdeSeparado(vPed,vProd: String): Double;
var
 qry: TIBQuery;
begin
 qry:= TIBQuery.Create(nil);
 qry.Database:= DMDados.IBDatabase;
 Result:= 0;
 try
 qry.Close;
 qry.SQL.Clear;
 qry.SQL.Add('select qtde_vendido,qtde_separado from item_pedido where pedv = :ped and cod_prod = :prod');
 qry.ParamByName('ped').AsString:= vPed;
 qry.ParamByName('prod').AsString:= vProd;
 qry.Open;

  Result:= qry.FieldByName('qtde_vendido').AsFloat - qry.FieldByName('qtde_separado').AsFloat;

  qry.Close;
 finally
   qry.Close;
   qry.Free;
  end;

end;

procedure TfrmSeparaProduto.CriaVolume(vPedido:String; vCarga: Integer);
 var
 qry: TIBQuery;
begin
 qry:= TIBQuery.Create(nil);
 qry.Database:=DMDados.IBDatabase;
try
qry.Close;
qry.SQL.Clear;
qry.SQL.Add('insert into volumes(id_carga,id_pedido,id_user,volume,data,status) ' +
            'values(:carga,:pedido,:user,:volume,:data,''A'')');

qry.ParamByName('carga').AsInteger  := vCarga;
qry.ParamByName('pedido').AsString  := vPedido;
qry.ParamByName('volume').AsInteger := PegaVolume(edtPedido.Caption,carga,'F') + 1;
qry.ParamByName('data').AsDateTime  := Date;
qry.ParamByName('user').AsInteger   := user;
qry.ExecSQL;

DMDados.IBTrans.CommitRetaining;

qry.Close;
finally
  qry.Close;
  qry.Free;
end;

end;

procedure TfrmSeparaProduto.AlteraEstoque(pProd: String;PQtde: Integer);
var
qry: TIBQuery;
begin
qry:=TIBQuery.Create(nil);
qry.Database:=DMDados.IBDatabase;
try
qry.Close;
qry.SQL.Clear;
qry.SQL.Add('update produtos set estoque_real = estoque_real - :qtde where cod = :cod');
qry.Params[0].AsInteger:=Pqtde;
qry.Params[1].AsString:=pProd;
qry.ExecSQL;
DMDados.IBTrans.CommitRetaining;
finally
  FreeAndNil(qry);
end;

end;

procedure TfrmSeparaProduto.btBuscaClick(Sender: TObject);
begin
IBItemPedidoProd.Locate('CBARRA',Trim(edtCodigo.Text),[]);
end;

procedure TfrmSeparaProduto.edtCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
TipoProd: String;
msg: String;
begin

  msg:= 'Para Iniciar uma separa��o,' + #13 +
        '� necessario abrir um volume!' +#13 + #13 +
        'Deseja abrir um volume para esse pedido?';

case Key  of

VK_RETURN:
begin
//confere qtde separada e se o produto existe no pedido
 if (existeProd) = True then
  begin
    if ConfereQtdeSeparado(edtPedido.Caption,vProd) = 0 then
   begin
      FrmExcessoProd.ShowModal;
      edtCodigo.Clear;
      edtCodigo.SetFocus;
      Abort;
   end;
  end
    else
 if (existeProd) = False then
    begin
    frmAtencao.ShowModal;
    edtCodigo.Clear;
      edtCodigo.SetFocus;
    Abort;
    end;


//confere estoque
   IBConfig.Close;
   IBConfig.Open;
   estoque:= ConfereEstoque(Trim(edtCodigo.Text));
if (estoque <= 0)and(IBConfigACEITAR_NEGATIVO_SAIDA.AsString = 'N')then
  begin
     frmErroEstoque.ShowModal;
     edtCodigo.Clear;
     edtCodigo.SetFocus;
     exit;
 end;



// se n�o existe volume para pedido, cria antes de separar
if ExisteVolume(IBItemPedidoProdPEDV.AsString) = False then
  if Dialogs.MessageDlg(msg,mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    begin
      CriaVolume(Trim(edtPedido.Caption),carga);
      ShowMessage('Volume criado com sucesso, enter para continuar...');
    end
     else
     Abort;



 if(edtCodigo.Text <> '')then
    begin
     if(existeProd)then
        begin
         if(pnlQtde.Visible = False)then
           begin
            separaProduto;
            FrmPrincipal.AtualizaItemCarga;
            edtCodigo.Clear;

             end
              else
                begin
                 ShowMessage('Digite uma qtde v�lida, � pressione ENTER...');
                 edtQtde.SetFocus;
                end;
         end
         else
          begin
          edtCodigo.Clear;
          frmAtencao.ShowModal;
              end;
   end
    else
     begin
     ShowMessage('� obrigatorio digitar um c�digo v�lido...');
     edtCodigo.SetFocus;
     end;

end;
end;
end;



procedure TfrmSeparaProduto.edtQtdeKeyPress(Sender: TObject; var Key: Char);
begin
if key = #13 then
begin
 try
   separaProduto;
   pnlQtde.Visible:=False;
   edtCodigo.SetFocus;
    finally
   edtCodigo.Clear;
   edtQtde.Clear;
    end;
end;
end;


function TfrmSeparaProduto.ExisteItemVolume(volume: Integer; pedido,
  produto: String): Boolean;
var
qry: TIBQuery;
cod: String;
begin
  qry:= TIBQuery.Create(nil);
  qry.Database:= DMDados.IBDatabase;
 try
  qry.Close;
  qry.SQL.Clear;
  qry.SQL.Add('select * from item_volumes where id_volume = :volume ' +
              'and id_pedido = :pedido and id_prod = :prod');
  qry.ParamByName('volume').AsInteger:= volume;
  qry.ParamByName('pedido').AsString:= pedido;
  qry.ParamByName('prod').AsString:= produto;
  qry.Open;

  cod:= qry.FieldByName('id_prod').AsString;

  if qry.IsEmpty then
   Result:= False
     else
   Result:= True;

  qry.Close;
  finally
  qry.Close;
  qry.Free;
  end;
end;

function TfrmSeparaProduto.existeProd: Boolean;
var
  qry: TIBQuery;
  SQL: String;
begin
  try
    vProd := '';
    tipo := '';
    qry := TIBQuery.Create(nil);
    qry.Database := DMDados.IBDatabase;

    SQL := 'select * from item_pedido where pedv = :pedido ';
    if Length(edtCodigo.Text) <= 6 then
      SQL := SQL + 'and cod_prod = :produto'
    else
    if Length(edtCodigo.Text) >  6  then
      SQL := SQL + 'and cbarra = :produto';
    qry.Close;
    qry.SQL.Clear;
    qry.SQL.Add(SQL);
    qry.Params[0].AsAnsiString := edtPedido.Caption;
    qry.Params[1].AsAnsiString := edtCodigo.Text;
    qry.Open;
    tipo := qry.FieldByName('unidade').AsString;
    if (tipo = 'MT') then
      pnlQtde.Visible := True
    else
      pnlQtde.Visible := False;
    if qry.IsEmpty then
    begin
      Result := False;
      vProd := '';
      vBarra:= '';
    end
    else
    begin
      Result := True;
      vProd:= qry.FieldByName('cod_prod').AsAnsiString;
      vBarra:= qry.FieldByName('cbarra').AsAnsiString;
    end;
  finally
    FreeAndNil(qry);
  end;
end;

function TfrmSeparaProduto.ExisteVolume(pedido: String; carga, volume: integer): Boolean;
var
qry: TIBQuery;
begin
  try
  qry:=TIBQuery.Create(nil);
  qry.Database:=DMDados.IBDatabase;

  qry.sql.clear;
  qry.SQL.Add('select * from  volumes where id_pedido = :pedido and id_carga = :carga and volume = :volume and status = ''F'' ');
  qry.ParamByName('pedido').AsString:= pedido;
  qry.ParamByName('carga').AsInteger:= carga;
  qry.ParamByName('volume').AsInteger:= volume;
  qry.Open;

    if qry.IsEmpty then
      Result:= False
       else
       Result:= True;

   qry.close;
  finally
   qry.close;
   qry.free;
  end;
end;

function TfrmSeparaProduto.ExisteVolume(pedido: String): Boolean;
var
qry: TIBQuery;
begin
  try
  qry:=TIBQuery.Create(nil);
  qry.Database:=DMDados.IBDatabase;

  qry.sql.clear;
  qry.SQL.Add('select status from volumes where id_pedido = :pedido');
  qry.ParamByName('pedido').AsString:= pedido;
  qry.Open;
  qry.last;


   if qry.FieldByName('status').AsString = 'A' then
      Result:= True
      else
      Result:= False;

   qry.close;
  finally
   qry.close;
   qry.free;
  end;
end;

procedure TfrmSeparaProduto.FecharVolume(vPedido: String);
var
qry:TIBQuery;
begin
 qry:=TIBQuery.Create(nil);
 qry.Database:=DMDados.IBDatabase;
 try
 qry.Close;
 qry.SQL.Clear;
 qry.SQL.Add('update volumes set status = ''F'' where id_pedido = :ped');
 qry.ParamByName('ped').AsString:= vPedido;
 qry.ExecSQL;

 qry.Close;
 finally
  qry.Close;
  qry.Free;
 end;
end;

procedure TfrmSeparaProduto.FormActivate(Sender: TObject);
begin
WindowState:=wsMaximized;
end;

procedure TfrmSeparaProduto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
separando:=False;
FrmPrincipal.IBItemPedido.Close;
FrmPrincipal.IBItemPedido.Open;
IBEstoque.Close;
end;

procedure TfrmSeparaProduto.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
if ExisteVolume(IBItemPedidoProdPEDV.AsString) = True then
 begin
   ShowMessage('O pedido ' + '[ ' + edtPedido.Caption + ' ]' + ', possui um volume em aberto' + #13#13 + '� obrigat�rio fechar o volume antes de sair...');
   CanClose:= False;
   end;
end;

procedure TfrmSeparaProduto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
 begin

case key of

VK_ESCAPE:
begin
  close;
end;

VK_F5:
begin
if Dialogs.MessageDlg('Deseja mesmo fechar o volume?',mtWarning, [mbYes, mbNo], 0, mbYes) = mrYes then
begin
  ImprimirEtiquetas;
  FecharVolume(edtPedido.Caption);
end;
end;

VK_F6:
begin
  pnlEtiqueta.Visible:= True;
  edtPedidoEtiqueta.Text:= edtPedido.Caption;
  edtCarga.Text:= lblCarga.Caption;
  edtVolumeEtiqueta.SetFocus;
end;

end;
end;

procedure TfrmSeparaProduto.FormShow(Sender: TObject);
begin
edtCodigo.SetFocus;
edtCodigo.Clear;
IBItemPedidoProd.Close;
IBItemPedidoProd.ParamByName('carga').AsInteger:=carga;
IBItemPedidoProd.ParamByName('pedido').AsAnsiString:=
FrmPrincipal.IBItemCarga.FieldByName('id_pedido').AsAnsiString;
lblCarga.Caption:=IntToStr(carga);
IBItemPedidoProd.Open;
IBEstoque.Open;
end;


procedure TfrmSeparaProduto.ImprimirEtiquetas;
begin

    // etiqueta de carga
    frxReportCarga.Variables['pedido'] := QuotedStr('PED: ' + edtPedido.Caption);
    frxReportCarga.Variables['carga'] := QuotedStr('CARGA: ' + lblCarga.Caption);
    frxReportCarga.Variables['volume'] := QuotedStr('VOL: ' + PegaVolume(edtPedido.Caption, StrToInt(lblCarga.Caption), 'A').ToString);
    frxReportCarga.Variables['cliente'] := QuotedStr(PegaDadosCliente(edtPedido.Caption, 'nome'));
    frxReportCarga.Variables['cidade'] := QuotedStr(PegaDadosCliente(edtPedido.Caption, 'cidade') + ' - ' +
      PegaDadosCliente(edtPedido.Caption, 'uf'));
    frxReportCarga.PrepareReport();
    frxReportCarga.Print;

    // itens separados
    IBProdPedido.Close;
    IBProdPedido.ParamByName('pedido').AsString := edtPedido.Caption;
    IBProdPedido.ParamByName('volume').AsInteger := PegaVolume(edtPedido.Caption, StrToInt(lblCarga.Caption), 'A');
    frxReportProdPedido.PrepareReport();
    frxReportProdPedido.Print;
end;

procedure TfrmSeparaProduto.InserirItemVolume(volume: Integer; qtd: Double);
var
qry:TIBQuery;
begin
 qry:=TIBQuery.Create(nil);
 qry.Database:=DMDados.IBDatabase;
 try
 if ExisteItemVolume(volume,IBItemPedidoProdPEDV.AsString,vProd) then
  begin
  qry.Close;
  qry.SQL.Clear;
  qry.SQL.Add('update item_volumes set qtde = qtde + :qtde ' +
              'where id_prod = :prod and id_volume = :volume and id_pedido = :pedido');
  qry.ParamByName('prod').AsString:= vProd;
  qry.ParamByName('volume').AsInteger:= volume;
  qry.ParamByName('qtde').AsFloat:= qtd;
  qry.ParamByName('pedido').AsString:= IBItemPedidoProdPEDV.AsString;
  qry.ExecSQL;
  DMDados.IBTrans.CommitRetaining;
  end
    else
  begin
  qry.Close;
  qry.SQL.Clear;
  qry.SQL.Add('insert into item_volumes(id_prod,id_volume,qtde,id_pedido) ' +
              'values(:prod,:volume,:qtde,:pedido)');
  qry.ParamByName('prod').AsString:= vProd;
  qry.ParamByName('volume').AsInteger:= volume;
  qry.ParamByName('qtde').AsFloat:= qtd;
  qry.ParamByName('pedido').AsString:= IBItemPedidoProdPEDV.AsString;
  qry.ExecSQL;
  DMDados.IBTrans.CommitRetaining;
  end;


  qry.Close;
 finally
  qry.Close;
  qry.Free;
 end;
end;

function TfrmSeparaProduto.PegaDadosCliente(vPedido,vCampo: String): String;
var
qry:TIBQuery;
begin
 qry:=TIBQuery.Create(nil);
 qry.Database:=DMDados.IBDatabase;
 try
  Result:= EmptyStr;

  qry.Close;
  qry.SQL.Clear;
  qry.SQL.Add('select '                                               +
              'c.' + vCampo                                           +
              ' from cadcli c '                                       +
              'inner join pedido p on(p.cod_cli = c.numero) '         +
              'where '                                                +
              'p.pedv = :ped ');
  qry.ParamByName('ped').AsString:= vPedido;
  qry.Open;

  if not qry.IsEmpty then
     Result:= qry.Fields[0].AsString;

  qry.Close;
 finally
  qry.Close;
  qry.Free;
  end;
 end;

function TfrmSeparaProduto.PegaVolume(vPedido: String; vCarga: Integer; tipo: String): Integer;
var
qry:TIBQuery;
begin
 qry:=TIBQuery.Create(nil);
 qry.Database:=DMDados.IBDatabase;
 try
  qry.Close;
  qry.SQL.Clear;
  qry.SQL.Add('select coalesce(max(v.volume),0)as volume_atual ' +
              'FROM VOLUMES V where v.id_carga = :carga and '    +
              'v.id_pedido = :ped and status = :tipo ');

  qry.ParamByName('carga').AsInteger:= carga;
  qry.ParamByName('ped').AsString:= IBItemPedidoProdPEDV.asstring;
  qry.ParamByName('tipo').AsString:= tipo;
  qry.Open;

  Result:= qry.FieldByName('volume_atual').AsInteger;

 qry.Close;
 finally
   qry.Close;
   qry.Free;
 end;

end;

procedure TfrmSeparaProduto.rdbOpcaoClick(Sender: TObject);
begin
edtCodigo.SetFocus;
end;

procedure TfrmSeparaProduto.separaProduto;
var
  qry: TIBQuery;
begin
  try
    qry := TIBQuery.Create(nil);
    qry.Database := DMDados.IBDatabase;

    if (pnlQtde.Visible = False)and (Length(edtCodigo.Text) <= 6) then
    begin
          qry.Close;
          qry.SQL.Clear;
          qry.SQL.Add('update item_pedido i set i.qtde_separado = i.qtde_separado + 1');
          qry.SQL.Add('where i.pedv = :pedido and i.cod_prod = :produto');
          qry.Params[0].AsAnsiString := IBItemPedidoProd.FieldByName('pedv').AsAnsiString;
          qry.Params[1].AsAnsiString := vProd;
          qry.ExecSQL;
          AlteraEstoque(vProd, 1);
          InserirItemVolume(PegaVolume(IBItemPedidoProdPEDV.AsString, carga,'A'), 1);

          DMDados.IBTrans.CommitRetaining;
          IBItemPedidoProd.Close;
          IBItemPedidoProd.Open;
          SetaProduto;
        end
         else
      if (pnlQtde.Visible = False)and(Length(edtCodigo.Text) > 6) then
        begin
        qry.Close;
          qry.SQL.Clear;
          qry.SQL.Add('update item_pedido i set i.qtde_separado = i.qtde_separado + 1');
          qry.SQL.Add('where i.pedv = :pedido and i.cbarra = :produto');
          qry.Params[0].AsAnsiString := IBItemPedidoProd.FieldByName('pedv').AsAnsiString;
          qry.Params[1].AsAnsiString := vBarra;
          qry.ExecSQL;
          AlteraEstoque(vProd,1);
          InserirItemVolume(PegaVolume(IBItemPedidoProdPEDV.AsString, carga,'A'), 1);

          DMDados.IBTrans.CommitRetaining;
          IBItemPedidoProd.Close;
          IBItemPedidoProd.Open;
          SetaProduto;
          end
          else
      if (pnlQtde.Visible = True)and(Length(edtCodigo.Text) <= 6) then
    begin
        qry.Close;
        qry.SQL.Clear;
        qry.SQL.Add('update item_pedido i set i.qtde_separado = i.qtde_separado + :qtde');
        qry.SQL.Add('where i.pedv = :pedido and i.cod_prod = :produto');
        qry.Params[0].AsInteger := StrToInt(edtQtde.Text);
        qry.Params[1].AsAnsiString := IBItemPedidoProd.FieldByName('pedv').AsAnsiString;
        qry.Params[2].AsAnsiString := vProd;
        qry.ExecSQL;

        AlteraEstoque(vProd, StrToInt(edtQtde.Text));
        InserirItemVolume(PegaVolume(IBItemPedidoProdPEDV.AsString, carga,'A'), StrToInt(edtQtde.Text));

        DMDados.IBTrans.CommitRetaining;
        IBItemPedidoProd.Close;
        IBItemPedidoProd.Open;
        SetaProduto
      end
       else
     if (pnlQtde.Visible = True)and(Length(edtCodigo.Text) > 6) then
    begin
        qry.Close;
        qry.SQL.Clear;
        qry.SQL.Add('update item_pedido i set i.qtde_separado = i.qtde_separado + :qtde');
        qry.SQL.Add('where i.pedv = :pedido and i.cbarra = :produto');
        qry.Params[0].AsInteger := StrToInt(edtQtde.Text);
        qry.Params[1].AsAnsiString := IBItemPedidoProd.FieldByName('pedv').AsAnsiString;
        qry.Params[2].AsAnsiString := vBarra;
        qry.ExecSQL;

        AlteraEstoque(vProd, StrToInt(edtQtde.Text));
        InserirItemVolume(PegaVolume(IBItemPedidoProdPEDV.AsString, carga,'A'), StrToInt(edtQtde.Text));

        DMDados.IBTrans.CommitRetaining;
        IBItemPedidoProd.Close;
        IBItemPedidoProd.Open;
        SetaProduto
    end;

    qry.Close;
  finally
    qry.Close;
    qry.Free;
  end;
end;

procedure TfrmSeparaProduto.SetaProduto;
begin
if Length(edtCodigo.Text) <= 6 then

 begin
 IBItemPedidoProd.Locate('cod_prod',vprod,[]);
   end
     else
if Length(edtCodigo.Text) > 6 then
  begin
 IBItemPedidoProd.Locate('cbarra',Trim(edtCodigo.Text),[]);
end;
end;

procedure TfrmSeparaProduto.Timer1Timer(Sender: TObject);
begin
IBEstoque.Close;
IBEstoque.Open;
end;


end.
