unit untPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DB, ExtCtrls, Grids, DBGrids,
  StdCtrls, ActnList,ShellApi,IniFiles, System.Actions, IBX.IBCustomDataSet, IBX.IBQuery;

type
  TFrmPrincipal = class(TForm)
    StatusBar1: TStatusBar;
    IBLogin: TIBQuery;
    IBLoginID_USER: TIntegerField;
    IBLoginNOME: TIBStringField;
    IBLoginLOGIN: TIBStringField;
    IBLoginSENHA: TIBStringField;
    IBLoginNIVEL: TIntegerField;
    Panel1: TPanel;
    Panel3: TPanel;
    gridCarga: TDBGrid;
    IBCarga: TIBQuery;
    Panel4: TPanel;
    dtCarga: TDataSource;
    IBCargaID_CARGA: TIntegerField;
    IBCargaNOME_REGIAO: TIBStringField;
    dtLogin: TDataSource;
    IBItemCarga: TIBQuery;
    dtItemCarga: TDataSource;
    gridPedido: TDBGrid;
    IBItemCargaID_PEDIDO: TIBStringField;
    Panel5: TPanel;
    IBItemPedido: TIBQuery;
    dtItemPedido: TDataSource;
    gridItemPedido: TDBGrid;
    IBItemPedidoCOD_PROD: TIBStringField;
    IBItemPedidoDESC_PROD: TIBStringField;
    IBItemPedidoCBARRA: TIBStringField;
    IBItemPedidoQTDE_VENDIDO: TIBBCDField;
    IBItemPedidoQTDE_SEPARADO: TIBBCDField;
    IBItemPedidoFALTA: TIBBCDField;
    IBItemCargaCIDADE: TIBStringField;
    IBItemCargaTOT_ITEM_PED: TIntegerField;
    TimerItemCarga: TTimer;
    ActionList1: TActionList;
    RedeAtualizaDados: TAction;
    RedeGravaDados: TAction;
    TimerCarga: TTimer;
    IBCargaFECHADO: TIBStringField;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edtCarga: TEdit;
    rdbPendente: TRadioButton;
    rdbTodas: TRadioButton;
    rdbFechadas: TRadioButton;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtCargaKeyPress(Sender: TObject; var Key: Char);
    procedure gridPedidoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TimerItemCargaTimer(Sender: TObject);
    procedure gridCargaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TimerCargaTimer(Sender: TObject);
    procedure gridPedidoDblClick(Sender: TObject);
    procedure rdbPendenteClick(Sender: TObject);
    procedure rdbFechadasClick(Sender: TObject);
    procedure rdbTodasClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
     { Private declarations }
  public
  procedure AtualizaItemCarga;
  procedure AtualizaCarga;
  procedure FiltraCarga(pFechou: string);
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;
  separando:Boolean;
  carga: Integer;
  user: integer;
  //variaveis impressora zebra
  arq,comand,bat: String;

implementation

uses untDMDados, untLogin, untSeparaProduto, untErro, untExcessoProd, untErroEstoque;

{$R *.dfm}

procedure TFrmPrincipal.gridCargaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
if (IBCarga.FieldByName('FECHADO').AsAnsiString = 'S') then
  begin
   gridCarga.Canvas.Font.Color:=clRed;
   gridCarga.Canvas.Font.Style:=[fsStrikeOut]; // coloque aqui a cor desejada
   gridCarga.DefaultDrawDataCell(Rect, gridCarga.columns[datacol].field, State);
  end;
end;

procedure TFrmPrincipal.gridPedidoDblClick(Sender: TObject);
begin
if(IBCarga.FieldByName('FECHADO').AsAnsiString = 'N')then
  begin
    carga:=0;
    carga:=IBCargaID_CARGA.AsInteger;
    separando:=True;
    frmSeparaProduto.ShowModal;
  end
   else
   begin
     ShowMessage('Carga j� Fechada, Impossivel altera��o...');
     exit;
   end;
end;

procedure TFrmPrincipal.gridPedidoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
cod: integer;
tot: double;
begin
tot:=0;
tot:= IBItemCarga.FieldByName('TOT_ITEM_PED').AsFloat;
if tot = 0  then
  begin
   gridPedido.Canvas.Font.Color:=clRed;
   gridPedido.Canvas.Font.Style:=[fsStrikeOut]; // coloque aqui a cor desejada
   gridPedido.DefaultDrawDataCell(Rect, gridPedido.columns[datacol].field, State);
  end;
end;



procedure TFrmPrincipal.rdbFechadasClick(Sender: TObject);
begin
FiltraCarga('S');
end;

procedure TFrmPrincipal.rdbPendenteClick(Sender: TObject);
begin
FiltraCarga('N');
end;

procedure TFrmPrincipal.rdbTodasClick(Sender: TObject);
var
sql: string;
begin
sql:='select CG.ID_CARGA,CG.FECHADO,R.NOME_REGIAO '+
     'from CARGA CG '+
     'inner join regiao R on(R.id_regiao = CG.id_regiao) '+
     'where CG. id_func = :id_user '+
     'order by CG.id_carga';
IBCarga.DisableControls;
IBCarga.Close;
IBCarga.SQL.Clear;
IBCarga.SQL.Add(sql);
IBCarga.ParamByName('id_user').AsInteger:=IBLoginID_USER.AsInteger;
IBCarga.Open;
IBCarga.EnableControls;
AtualizaItemCarga;
end;

procedure TFrmPrincipal.TimerCargaTimer(Sender: TObject);
begin
AtualizaCarga;
end;

procedure TFrmPrincipal.TimerItemCargaTimer(Sender: TObject);
begin
AtualizaItemCarga;
end;

procedure TFrmPrincipal.AtualizaCarga;
var
bok: TBookmark;
begin
if separando = False then
 bok:=IBCarga.GetBookmark;
 IBCarga.DisableControls;
 IBCarga.Active:=False;
 IBCarga.Active:=True;
 IBCarga.EnableControls;
 IBCarga.GotoBookmark(bok);
 IBCarga.FreeBookmark(bok);
end;

procedure TFrmPrincipal.AtualizaItemCarga;
var
bok: TBookmark;
begin
if separando = false then
 bok:=IBItemCarga.GetBookmark;
 IBItemCarga.DisableControls;
 IBItemCarga.Active:=False;
 IBItemCarga.Active:=True;
 IBItemCarga.EnableControls;
 IBItemCarga.GotoBookmark(bok);
 IBItemCarga.FreeBookmark(bok);
end;

procedure TFrmPrincipal.Button1Click(Sender: TObject);
begin
frmErroEstoque.ShowModal;
end;

procedure TFrmPrincipal.edtCargaKeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then
 begin
IBCarga.Locate('id_carga',StrToInt(edtCarga.Text),[]);
if IBCarga.IsEmpty then
    showmessage('Carga inexistente, tente novamente...');
    edtCarga.SetFocus;
    edtCarga.Clear;
 end;
end;

procedure TFrmPrincipal.FiltraCarga(pFechou: string);
var
sql: string;
begin
sql:='select CG.ID_CARGA,CG.FECHADO,R.NOME_REGIAO '+
     'from CARGA CG '+
     'inner join regiao R on(R.id_regiao = CG.id_regiao) '+
     'where CG. id_func = :id_user and CG.fechado = :fechado '+
     'order by CG.id_carga';
IBCarga.DisableControls;
IBCarga.Close;
IBCarga.SQL.Clear;
IBCarga.SQL.Add(sql);
IBCarga.ParamByName('id_user').AsInteger:=IBLogin.FieldByName('id_user').AsInteger;
IBCarga.ParamByName('fechado').AsAnsiString:=pFechou;
IBCarga.Open;
IBCarga.EnableControls;
AtualizaItemCarga;
end;

procedure TFrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
IBCarga.Close;
IBItemCarga.Close;
IBItemPedido.Close;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
var
ini:TIniFile;
begin
ini    := TIniFile.Create('C:\PlastNova\config.ini');
arq    := ini.ReadString('ETIQUETA','ARQ','');
comand := ini.ReadString('ETIQUETA','COMAND','');
bat    := ini.ReadString('ETIQUETA','BAT','');
IBCarga.Open;
IBItemCarga.Open;
IBItemPedido.Open;
FrmPrincipal.WindowState:=wsMaximized;
separando:=False;
end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
frmLogin.ShowModal;
user:= IBLoginID_USER.AsInteger;
end;



end.
