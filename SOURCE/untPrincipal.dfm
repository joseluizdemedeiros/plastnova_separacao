object FrmPrincipal: TFrmPrincipal
  Left = 228
  Top = 142
  Caption = 'Sistema Separa'#231#227'o - PlastNova'
  ClientHeight = 536
  ClientWidth = 902
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 493
    Width = 902
    Height = 43
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    Panels = <
      item
        Alignment = taCenter
        Width = 650
      end
      item
        Width = 250
      end>
    UseSystemFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 902
    Height = 493
    Align = alClient
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 73
      Width = 304
      Height = 419
      Align = alLeft
      TabOrder = 0
      object gridCarga: TDBGrid
        Left = 1
        Top = 1
        Width = 302
        Height = 417
        Align = alClient
        Color = clMoneyGreen
        DataSource = dtCarga
        DrawingStyle = gdsGradient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = gridCargaDrawColumnCell
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'ID_CARGA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Title.Caption = 'CARGA'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -21
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 78
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'NOME_REGIAO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'REGI'#195'O'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -21
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 187
            Visible = True
          end>
      end
    end
    object Panel4: TPanel
      Left = 305
      Top = 73
      Width = 416
      Height = 419
      Align = alLeft
      TabOrder = 1
      object gridPedido: TDBGrid
        Left = 1
        Top = 1
        Width = 414
        Height = 417
        Align = alClient
        Color = clGradientActiveCaption
        DataSource = dtItemCarga
        DrawingStyle = gdsGradient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = gridPedidoDrawColumnCell
        OnDblClick = gridPedidoDblClick
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'ID_PEDIDO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Title.Caption = 'PEDIDO'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clMaroon
            Title.Font.Height = -21
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'CIDADE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -21
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlack
            Title.Font.Height = -19
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 282
            Visible = True
          end>
      end
    end
    object Panel5: TPanel
      Left = 721
      Top = 73
      Width = 180
      Height = 419
      Align = alClient
      TabOrder = 2
      object gridItemPedido: TDBGrid
        Left = 1
        Top = 1
        Width = 178
        Height = 417
        Align = alClient
        Color = clInactiveBorder
        DataSource = dtItemPedido
        DrawingStyle = gdsGradient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'COD_PROD'
            Title.Caption = 'PROD.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -19
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'DESC_PROD'
            Title.Caption = 'DESCRI'#199#195'O'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -19
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 270
            Visible = True
          end
          item
            Alignment = taCenter
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'QTDE_VENDIDO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'VENDEU'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -19
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 75
            Visible = True
          end
          item
            Alignment = taCenter
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'QTDE_SEPARADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'SEPAR.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -19
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 62
            Visible = True
          end
          item
            Alignment = taCenter
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'FALTA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -19
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -19
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 63
            Visible = True
          end>
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 900
      Height = 72
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      BevelWidth = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
      object Label1: TLabel
        Left = 56
        Top = 8
        Width = 125
        Height = 33
        Caption = 'Carga(s) '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 440
        Top = 8
        Width = 139
        Height = 33
        Caption = 'Pedido(s) '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 873
        Top = 8
        Width = 247
        Height = 33
        Caption = 'Item(s) do Pedido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 440
        Top = 55
        Width = 180
        Height = 13
        Caption = 'Atualiza'#231#227'o a cada 30 segundos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 5
        Top = 55
        Width = 174
        Height = 13
        Caption = 'Atualiza'#231#227'o a cada 3 minutos...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtCarga: TEdit
        Left = 187
        Top = 10
        Width = 67
        Height = 31
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        NumbersOnly = True
        ParentFont = False
        TabOrder = 0
        OnKeyPress = edtCargaKeyPress
      end
      object rdbPendente: TRadioButton
        Left = 272
        Top = 4
        Width = 113
        Height = 17
        Caption = 'Pendentes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = rdbPendenteClick
      end
      object rdbTodas: TRadioButton
        Left = 272
        Top = 27
        Width = 113
        Height = 17
        Caption = 'Todas'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        TabStop = True
        OnClick = rdbTodasClick
      end
      object rdbFechadas: TRadioButton
        Left = 272
        Top = 49
        Width = 113
        Height = 17
        Caption = 'Fechadas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = rdbFechadasClick
      end
      object Button1: TButton
        Left = 616
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 4
        OnClick = Button1Click
      end
    end
  end
  object IBLogin: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select * from USUARIO')
    Left = 48
    Top = 96
    object IBLoginID_USER: TIntegerField
      FieldName = 'ID_USER'
      Origin = '"USUARIO"."ID_USER"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IBLoginNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"USUARIO"."NOME"'
      Size = 40
    end
    object IBLoginLOGIN: TIBStringField
      FieldName = 'LOGIN'
      Origin = '"USUARIO"."LOGIN"'
      Size = 10
    end
    object IBLoginSENHA: TIBStringField
      FieldName = 'SENHA'
      Origin = '"USUARIO"."SENHA"'
      Size = 6
    end
    object IBLoginNIVEL: TIntegerField
      FieldName = 'NIVEL'
      Origin = '"USUARIO"."NIVEL"'
    end
  end
  object IBCarga: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = True
    DataSource = dtLogin
    ParamCheck = True
    SQL.Strings = (
      'select CG.ID_CARGA,CG.FECHADO,R.NOME_REGIAO'
      'from CARGA CG'
      'inner join regiao R on(R.id_regiao = CG.id_regiao)'
      'where CG. id_func = :id_user'
      'and fechado = '#39'N'#39
      'order by CG.id_carga')
    Left = 89
    Top = 98
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_USER'
        ParamType = ptInput
        Size = 4
      end>
    object IBCargaID_CARGA: TIntegerField
      Alignment = taCenter
      FieldName = 'ID_CARGA'
      Origin = '"CARGA"."ID_CARGA"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IBCargaNOME_REGIAO: TIBStringField
      FieldName = 'NOME_REGIAO'
      Origin = '"REGIAO"."NOME_REGIAO"'
      Size = 60
    end
    object IBCargaFECHADO: TIBStringField
      FieldName = 'FECHADO'
      Origin = '"CARGA"."FECHADO"'
      FixedChar = True
      Size = 1
    end
  end
  object dtCarga: TDataSource
    DataSet = IBCarga
    Left = 81
    Top = 146
  end
  object dtLogin: TDataSource
    DataSet = IBLogin
    Left = 25
    Top = 146
  end
  object IBItemCarga: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = True
    DataSource = dtCarga
    ParamCheck = True
    SQL.Strings = (
      'select IG.ID_PEDIDO,P.CIDADE,tot_item_ped'
      ' from ITEM_CARGA IG'
      'INNER JOIN PEDIDO P ON(P.PEDV = IG.ID_PEDIDO)'
      'where IG.id_carga = :id_carga'
      'order by IG. id_pedido')
    Left = 161
    Top = 98
    ParamData = <
      item
        DataType = ftInteger
        Name = 'id_carga'
        ParamType = ptInput
      end>
    object IBItemCargaID_PEDIDO: TIBStringField
      Alignment = taCenter
      FieldName = 'ID_PEDIDO'
      Origin = '"ITEM_CARGA"."ID_PEDIDO"'
      Size = 6
    end
    object IBItemCargaCIDADE: TIBStringField
      FieldName = 'CIDADE'
      Origin = '"PEDIDO"."CIDADE"'
      Size = 30
    end
    object IBItemCargaTOT_ITEM_PED: TIntegerField
      FieldName = 'TOT_ITEM_PED'
      Origin = '"ITEM_CARGA"."TOT_ITEM_PED"'
    end
  end
  object dtItemCarga: TDataSource
    DataSet = IBItemCarga
    Left = 145
    Top = 146
  end
  object IBItemPedido: TIBQuery
    Database = DMDados.IBDatabase
    Transaction = DMDados.IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    DataSource = dtItemCarga
    ParamCheck = True
    SQL.Strings = (
      'select '
      'I.COD_PROD,I.DESC_PROD,I.cbarra, '
      'I.QTDE_VENDIDO,'
      'I. QTDE_SEPARADO,'
      'sum(I.qtde_vendido - I.qtde_separado)as FALTA'
      'from ITEM_PEDIDO I'
      'inner join Pedido P on(P.pedv = I.pedv)'
      'where'
      ' P.PEDV = :ID_PEDIDO'
      'group by'
      'I.COD_PROD,I.DESC_PROD,I.cbarra, '
      'I.QTDE_VENDIDO,'
      'I. QTDE_SEPARADO'
      'order by'
      ' I.desc_prod')
    Left = 217
    Top = 98
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_PEDIDO'
        ParamType = ptInput
      end>
    object IBItemPedidoCOD_PROD: TIBStringField
      FieldName = 'COD_PROD'
      Origin = '"ITEM_PEDIDO"."COD_PROD"'
      FixedChar = True
      Size = 4
    end
    object IBItemPedidoDESC_PROD: TIBStringField
      FieldName = 'DESC_PROD'
      Origin = '"ITEM_PEDIDO"."DESC_PROD"'
      Size = 100
    end
    object IBItemPedidoCBARRA: TIBStringField
      FieldName = 'CBARRA'
      Origin = '"ITEM_PEDIDO"."CBARRA"'
      Size = 13
    end
    object IBItemPedidoQTDE_VENDIDO: TIBBCDField
      FieldName = 'QTDE_VENDIDO'
      Origin = '"ITEM_PEDIDO"."QTDE_VENDIDO"'
      Precision = 18
      Size = 2
    end
    object IBItemPedidoQTDE_SEPARADO: TIBBCDField
      FieldName = 'QTDE_SEPARADO'
      Origin = '"ITEM_PEDIDO"."QTDE_SEPARADO"'
      Precision = 18
      Size = 2
    end
    object IBItemPedidoFALTA: TIBBCDField
      FieldName = 'FALTA'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
  end
  object dtItemPedido: TDataSource
    DataSet = IBItemPedido
    Left = 217
    Top = 146
  end
  object TimerItemCarga: TTimer
    Interval = 30000
    OnTimer = TimerItemCargaTimer
    Left = 41
    Top = 226
  end
  object ActionList1: TActionList
    Left = 153
    Top = 210
    object RedeAtualizaDados: TAction
      Caption = 'RedeAtualizaDados'
    end
    object RedeGravaDados: TAction
      Caption = 'RedeGravaDados'
    end
  end
  object TimerCarga: TTimer
    Interval = 180000
    OnTimer = TimerCargaTimer
    Left = 41
    Top = 282
  end
end
