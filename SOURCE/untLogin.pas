unit untLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, pngimage, ExtCtrls, IBX.IBQuery;

type
  TfrmLogin = class(TForm)
    Panel1: TPanel;
    btLogin: TBitBtn;
    btFechar: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    valorSenha: TEdit;
    Image1: TImage;
    ValorNome: TComboBox;
    procedure btFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btLoginClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure valorSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ValorNomeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;
  acao: Boolean;

implementation

uses untPrincipal, untDMDados;

{$R *.dfm}

procedure TfrmLogin.btFecharClick(Sender: TObject);
begin
acao:=True;
FrmPrincipal.Close;
end;

procedure TfrmLogin.btLoginClick(Sender: TObject);
var
 mensagem: String;
begin

  frmPrincipal.IBLogin.Close;
  frmPrincipal.IBLogin.SQL.Clear;
  frmPrincipal.IBLogin.SQL.Add('SELECT * FROM USUARIO U');
  frmPrincipal.IBLogin.SQL.Add('WHERE U.LOGIN = :PLOGIN AND U.SENHA = :PSENHA');
  frmPrincipal.IBLogin.ParamByName('PLOGIN').AsAnsiString:=ValorNome.Text;
  frmPrincipal.IBLogin.ParamByName('PSENHA').AsAnsiString:=ValorSenha.Text;
  frmPrincipal.IBLogin.open;

 if (frmPrincipal.IBLogin.RecordCount)= 1 then
  begin
   frmPrincipal.StatusBar1.Panels[0].Text:='' + 'Us�ario Logado:  ' +
       frmLogin.ValorNome.Text;
            acao:=true;
            frmLogin.close;

   end
      else

   if (frmPrincipal.IBLogin.RecordCount) = 0 then
  begin
    mensagem:= 'Nome ou Senha do Us�ario inv�lido.' +  #13 + #13 +
    'Se voc� esqueceu sua senha. Consulte ' + #13 + ' o administrator do sistema.';

    Application.MessageBox(pChar(mensagem),'Login n�o autorizado', MB_OK
      +MB_ICONERROR);
      ValorNome.Text:='';
      ValorSenha.Text:= '';

  end;
end;
procedure TfrmLogin.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
Canclose:=acao;
end;

procedure TfrmLogin.FormCreate(Sender: TObject);
var
qry: TIBQuery;
begin
try
qry:=TIBQuery.Create(nil);
qry.Database:=DMDados.IBDatabase;

qry.Close;
qry.SQL.Clear;
qry.SQL.Add('select * from usuario order by login');
qry.Open;
qry.First;

while not qry.eof do
  begin
    ValorNome.Items.Add(qry.FieldByName('login').AsAnsiString);
    qry.Next;
  end;
acao:=False;
finally
FreeAndNil(qry);
end;
end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
valorNome.SetFocus;
end;

procedure TfrmLogin.ValorNomeClick(Sender: TObject);
begin
valorSenha.SetFocus;
end;

procedure TfrmLogin.valorSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case key  of

VK_RETURN:
btLogin.Click;
end;
end;

end.
